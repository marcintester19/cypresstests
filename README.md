# CypressTests test
Install cypress:
```
npm install cypress --save-dev
```

Run all tests:

```
npx cypress run
```
Run ui tests:
```
npm run uiTests
```
Run api tests:
```
npm run apiTests
```
Run tests on specific browser: chrome, edge, firefox:
```
npm run uiTests -- --browser chrome
```
